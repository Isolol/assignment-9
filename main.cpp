#include <iostream>


double averageValues(int arrayOfValues[], int inputElements)
{
	double sumOfValues = 0;
	double averageValue = 0;

	for(int i = 0; i <= inputElements - 1; i++)
	{
		sumOfValues += arrayOfValues[i];
	}

	averageValue = sumOfValues / inputElements;

	return averageValue;
}

void minimumValueFunct(int arrayOfValues[], int inputElements, int *minimumValuePtr)
{
	*minimumValuePtr = arrayOfValues[0];
	for(int i = 1; i < inputElements; i++)
	{
		if(arrayOfValues[i] < *minimumValuePtr)
		{
			*minimumValuePtr = arrayOfValues[i];
		}
	}
}

int main()
{
	int inputNumberOfElements = 0;

	int arrayOfValues[20] = {0};
	
	int minimumValue = 0;

	do 
	{
		std::cout << "How many values would you like to enter? (1 to 20)" << std::endl;
		std::cin >> inputNumberOfElements;
		std::cout << std::endl;
	} while (inputNumberOfElements < 1 || inputNumberOfElements > 20);

	for(int i = 0; i <= inputNumberOfElements - 1; i++)
	{
		int elementValueInput = 0;
		std::cout << "Please enter the value for element number: " << i + 1 << std::endl;
		std::cin >> elementValueInput;
		std::cout << std::endl;
		arrayOfValues[i] = elementValueInput;
	}
	
	minimumValueFunct(arrayOfValues, inputNumberOfElements, &minimumValue);

	std::cout << "The average of the values given is: " << averageValues(arrayOfValues, inputNumberOfElements) << std::endl; 
	std::cout << "The minimum value of values given is: " << minimumValue << std::endl;
}